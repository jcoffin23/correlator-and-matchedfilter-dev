#!/usr/bin/env python3

# File: Transmitter.py

"""Transmitter Module


"""
import numpy as np
import math

from common import (ENC_HEADER, TOC, FRAME_HEADER, TRAINING_MSG,
                    PREAMBLE_BYTES, SYNC_BYTES, P_MOD,
                    divide_subcarriers)
from signals import rrc_pulse, rc_pulse
from txrx_utils import insert_OFDM, unpack_bytes, pulse_shaping, freq_shift
from PHY_Tables import ModTable, CodeTable
from OFDM import OFDM


#
# Helper functions
#
#
# used by scheduler to determine which packets can be transmitted
#
# TODO: Use this function in build_ofdm_frame!
def tx_n_symbols(n_bytes, mod, code, n_pkts=0):
    """figure out how many symbols n_bytes produce for given modulation
       and coding"""
    n_bits = 8*n_bytes

    # account for encapsulation header
    n_bits += n_pkts*ENC_HEADER.msg_size*8

    # TODO: account for coding
    n_coded_bits = n_bits

    # modulation
    bps = ModTable[mod]['bits_per_sym']
    n_syms = math.ceil(n_coded_bits/bps)

    return n_syms


def tx_pld_avail(dur_s, n_bands, n_bc_syms, fs, guard_time,
                 n_fft, n_cp, n_train, pre_fsT, pre_nT, toc_mod):
    """determine how many symbols (TF bins) are available to carry the
    payload
    """
    # total number of samples
    N = (dur_s-guard_time)*fs

    # header samples
    Ns = (len(PREAMBLE_BYTES) + len(SYNC_BYTES) +
          FRAME_HEADER.msg_size + pre_nT-1)*pre_fsT

    # number of OFDM blocks
    N_OFDM = int((N-Ns)/(n_fft+n_cp))

    # number of OFDM blocks for ToC
    n_syms_TOC = tx_n_symbols(TOC.msg_size, toc_mod, 0)
    N_TOC = math.ceil(n_syms_TOC/n_bands)

    # number of OFDM blocks for BC
    N_BC = math.ceil(n_bc_syms/n_bands)

    n_pld = (N_OFDM - n_train - N_TOC - N_BC - 1)*n_bands

    return n_pld
#
# function that encapsulates all packets to a given
# destination and turns them into a stream for each destination
#
# Note the input payload is equal to instr['payload'] below


def make_streams(payload, buffer):
    """ encapsulate multiple packets in a byte stream

    Packets are sent back-to-back and prepended by a header that
    faciliates separating them at the receiver (see ENC_HEADER).

    Input `payload` is an array of dictionaries that describes the packets to
    be combined. An example looks like this:

    [{'next_hop_id': 2, 'modulation': 2, 'code': 0,
              'packets': [{'pkt_key': 98, 'n_bytes': 124, 'offset': 2152, 'ToA_ns': 1511792169819767808},
                          {'pkt_key': 99, 'n_bytes': 124, 'offset': 2276, 'ToA_ns': 1511792169919767808}]},
             {'next_hop_id': 3, 'modulation': 2, 'code': 0,
              'packets': [{'pkt_key': 96, 'n_bytes': 124, 'offset': 1904, 'ToA_ns': 1511792170019767808},
                          {'pkt_key': 97, 'n_bytes': 124, 'offset': 2028, 'ToA_ns': 1511792170119767808}]},
             {'next_hop_id': 3, 'modulation': 2, 'code': 0,
              'packets': [{'pkt_key': 90, 'n_bytes': 1240, 'offset': 1000, 'ToA_ns': 1511792170219767808},
                          {'pkt_key': 91, 'n_bytes': 1240, 'offset': 1124, 'ToA_ns': 1511792170319767808}]}]

    The input `buffer` holds the actual packets. The `offset` and `n_bytes`
    keys in the payload dictionaries describe the location of packets in the
    buffer.
    """
    # step 1: gather all packets for the same destinationn
    pkts_for = {}
    mod_for = {}
    code_for = {}

    for flow in payload:
        next_hop = flow['next_hop_id']
        try:
            pkts = pkts_for[next_hop]
        except KeyError:
            pkts = pkts_for[next_hop] = []
            mod_for[next_hop] = flow['modulation']
            code_for[next_hop] = flow['code']

        for pkt in flow['packets']:
            pkts.append(pkt)

    # step 2: concatenate pkts indicated in the lists that we
    # just constructed into a byte stream
    streams_for = {}
    ENC_HDR_SIZE = ENC_HEADER.msg_size

    for dest, pkts in pkts_for.items():
        n_pkts = len(pkts)
        n_bytes = sum([pkt['n_bytes'] for pkt in pkts])

        # pre-allocate space for stream
        stream = np.empty(n_bytes+n_pkts*ENC_HDR_SIZE,
                          dtype=np.uint8)

        start = 0
        for pkt in pkts:
            n_bytes = pkt['n_bytes']
            offset = pkt['offset']

            # construct an encapsulation header from the pkt length
            enc_hdr = ENC_HEADER.encode(n_bytes)

            # insert in stream
            enc_hdr = np.fromiter(enc_hdr, dtype=np.byte)
            stream[start:start+ENC_HDR_SIZE] = enc_hdr
            start += ENC_HDR_SIZE

            stream[start:start+n_bytes] = buffer[offset:offset+n_bytes]
            start += n_bytes

        streams_for[dest] = stream

    return (streams_for, mod_for, code_for)


class Transmitter(object):
    """Class to encapsulate transmitter functionality"""
    def __init__(self,
                 nFFT=256,
                 howmany=64,
                 l_cp=8,
                 n_block_training=1,
                 training_mod=1,
                 preamble_beta=0.75,     # pulse roll-off
                 preamble_fsT=16,        # up-sampling of preamble -> BW
                 preamble_nT=4,          # pulse length in symbol periods
                 preamble_pulse='rrc'):  # pulse-type either 'rc' or 'rrc'
        """ Constructor - sets configuration parameters """

        self.nFFT = nFFT

        # number of simultaneous FFTs - not OFDM blocks in frame
        self.howmany = howmany
        self.l_cp = l_cp

        # set up the FFTW machinery
        self.ofdm = OFDM(nFFT, howmany)

        # training sequence
        self.n_block_training = n_block_training
        self.training_mod = training_mod

        # repeat TRAINING MSG often enough that we can always fill training
        # blocks
        rep = math.ceil(len(TRAINING_MSG)*8/(n_block_training*nFFT))

        training_bits = np.empty(8*rep*len(TRAINING_MSG), dtype=np.uint8)
        unpack_bytes(TRAINING_MSG*rep,
                     ModTable[training_mod]['bits_per_sym'],
                     training_bits)
        modulator = ModTable[training_mod]['modulator_obj']
        self.training_syms = modulator.modulate(training_bits)

        # preamble and frame header related parameters
        self.preamble_n_bytes = n_bytes = (len(PREAMBLE_BYTES) +
                                           len(SYNC_BYTES) +
                                           FRAME_HEADER.msg_size)
        self.preamble_n_syms = int(n_bytes*8/ModTable[P_MOD]['bits_per_sym'])

        self.preamble_beta = preamble_beta
        self.preamble_fsT = preamble_fsT
        self.preamble_nT = preamble_nT
        if preamble_pulse == 'rrc':
            self.preamble_pulse_sig = rrc_pulse(preamble_beta,
                                                preamble_fsT,
                                                preamble_nT)
        elif preamble_pulse == 'rc':
            self.preamble_pulse_sig = rc_pulse(preamble_beta,
                                               preamble_fsT,
                                               preamble_nT)
        else:
            raise ValueError("pulse type must be either 'rc' or 'rrc'.")

    def work(self, instr, packet_buffer):
        # assemble the OFDM frame first since we need its length
        # for the header

        # if __debug__:
        #     print("tx.work: before build_ofdm_frame")

        ofdm_frame = self.build_ofdm_frame(instr, packet_buffer)
        n_blocks = ofdm_frame.shape[0]

        k_lo = instr['f_low']
        k_hi = instr['f_high']

        nBands = k_hi - k_lo

        # compute preamble and frame header (see common for structure
        # of FRAME_HEADER)
        # TOC mod_code and number of blocks
        # if __debug__:
        #     print("tx.work: before assembling FRAME_HEADER")

        mod_code_toc = 16*instr['toc_mod'] + 0  # no FEC in TOC
        n_bytes_toc = TOC.msg_size
        mod_toc = instr['toc_mod']
        bps_toc = ModTable[mod_toc]['bits_per_sym']
        code_toc = 0   # no coding, using CRC
        rate_toc = CodeTable[code_toc]['rate']

        n_syms_toc = math.ceil(n_bytes_toc*8/(bps_toc*rate_toc))
        n_block_toc = math.ceil(n_syms_toc/nBands)

        # parameters passed here are used to construct the header data;
        # they must be passed in exactly the order specified by FRAME_HEADER!
        # if __debug__:
        #     print("tx.work: before build_preamble_header")

        ph_sig = self.build_preamble_header(k_lo, k_hi,
                                            n_blocks, n_block_toc,
                                            mod_code_toc)

        # frequency shift the preamble and header
        # note: the OFDM part will be placed in the correct band by filling
        # the proper subcarriers
        ph_sig = freq_shift(ph_sig,
                            instr['f_center'],
                            instr['fs'])

        # compute how long the IQ signal will be and allocate storage for it
        ph_len = ph_sig.shape[0]
        out_vec = np.empty(n_blocks*(self.nFFT+self.l_cp) + ph_len,
                           dtype=np.complex64)

        # insert the preamble
        out_vec[:ph_len] = ph_sig

        # if __debug__:
        #    print("tx.work: before ofdm")
        # OFDM of the frame assembled above ...
        ofdm_out = np.empty_like(ofdm_frame)
        self.ofdm.ofdm_mod(ofdm_frame, ofdm_out)

        # again, we must ensure that we're not clipping
        scale = np.max(np.abs(ofdm_out))

        # p->s conversion (with scaling)
        self.ofdm.p_to_s(ofdm_out*1/scale, out_vec[ph_len:], self.l_cp)

        return out_vec

    def build_preamble_header(self, *args):
        """Construct the preamble and header signal

        The signal is BPSK modulated with pulse shaping to control bandwidth.

        The header encodes information specified by FRAME_HEADER; the
        arguments passed to this function are simply passed to the encoder
        for FRAME_HEADER.
        """
        hdr_bytes = FRAME_HEADER.encode(*args)

        # Modulation
        b = np.empty(self.preamble_n_syms, dtype=np.uint8)
        unpack_bytes(bytearray(PREAMBLE_BYTES + SYNC_BYTES + hdr_bytes),
                     ModTable[P_MOD]['bits_per_sym'],
                     b)
        syms = ModTable[P_MOD]['modulator_obj'].modulate(b)

        # pulse shaping
        ph_sig = pulse_shaping(syms,
                               self.preamble_pulse_sig,
                               self.preamble_fsT)

        # we must ensure that he signal won't clip -> scale by max. magnitude
        # the base of ph_sig is a numpy array (ph_sig is a memoryview)
        scale = np.max(np.abs(ph_sig.base.real))

        return ph_sig.base/scale

    def build_ofdm_frame(self, instr, packet_buffer):
        """ Given a dictionary `instr` with instructions for assembling a frame
        and a reference to a buffer `packet_buffer` containing packets,
        build the frame to be sent.

        The contentents of the `instr` dictionary and the structure of the
        frame is documented in `TX_Interface.json`.

        Usage:
            tx = Transmitter(nFFT)
            IQsamples = tx.work(instr, buffer)
        """

        # collect pertinent parameters from instr
        f_lo = instr['f_low']
        f_hi = instr['f_high']

        nBands = f_hi - f_lo

        #
        # Plan how to assemble the pieces that must be combined
        #
        # 1. data streams (with the help of make_streams)
        data_for, mod_for, code_for = make_streams(instr['payload'],
                                                   packet_buffer)

        # figure out how many symbols in each stream
        n_syms_for = {}
        data_for_toc = []
        n_dest = 0

        # ensure deterministic order of dest here and when filling below
        for dest in sorted(data_for.keys()):
            n_bytes = len(data_for[dest])

            if n_bytes == 0:
                continue

            n_dest += 1

            mod = mod_for[dest]
            bps = ModTable[mod]['bits_per_sym']
            code = code_for[dest]
            rate = CodeTable[code]['rate']

            n_syms = math.ceil(n_bytes*8.0/(bps*rate))
            n_syms_for[dest] = n_syms
            modcode = 16*mod + code

            # store data needed for TOC
            data_for_toc.extend((dest, modcode, n_bytes))

        # how many subcarriers should be assigned for each destination
        if n_dest > 0:
            n_subs_for, n_block_data = divide_subcarriers(nBands, n_syms_for)
        else:
            n_subs_for = {}
            n_block_data = 0

        # if __debug__:
        #     print("> build_ofdm_frame: data blocks", n_block_data)
        #     for d in n_subs_for.keys():
        #         print(">> ", n_syms_for[d], n_subs_for[d])

        # 2. similarly for the BC data
        bc_data = instr['bc_header']
        n_bytes_bc = len(bc_data['bc_payload'])
        mod_bc = bc_data['modulation']
        bps_bc = ModTable[mod_bc]['bits_per_sym']
        code_bc = bc_data['code']
        rate_bc = CodeTable[code_bc]['rate']
        modcode_bc = 16*mod_bc + code_bc

        n_syms_bc = math.ceil(n_bytes_bc*8/(bps_bc*rate_bc))
        n_block_bc = math.ceil(n_syms_bc/nBands)

        # number of unused bins
        n_pad_syms_bc = n_block_bc*nBands - n_syms_bc
        n_pad_bytes_bc = int(n_pad_syms_bc*bps_bc*rate_bc/8)

        # 3. TOC
        n_bytes_toc = TOC.msg_size
        mod_toc = instr['toc_mod']
        bps_toc = ModTable[mod_toc]['bits_per_sym']
        code_toc = 0   # no coding, using CRC
        rate_toc = CodeTable[code_toc]['rate']

        n_syms_toc = math.ceil(n_bytes_toc*8/(bps_toc*rate_toc))
        n_block_toc = math.ceil(n_syms_toc/nBands)

        # number of unused bins
        n_pad_syms_toc = n_block_toc*nBands - n_syms_toc
        n_pad_bytes_toc = int(n_pad_syms_toc*bps_toc*rate_toc/8)

        # allocate space for the frame, now that we know how long it is
        n_blocks = (n_block_data +
                    n_block_bc +
                    n_block_toc +
                    self.n_block_training)

        ofdm_frame = np.zeros((n_blocks, self.nFFT),
                              dtype=np.complex64)

        # print(n_block_data, n_block_toc)
        #
        # Modulate information and insert in in OFDM frame with the help of
        # insert_OFDM
        #

        m_start = 0  # keeps track of OFDM block

        # 1. Training sequence: fill n_block_training from the training
        # sequence
        # if __debug__:
        #     print(">build_ofdm_frame: insert training - ",
        #           self.n_block_training*nBands,
        #           f_lo, f_hi, m_start)

        insert_OFDM(ofdm_frame,
                    self.training_syms[:self.n_block_training*nBands],
                    f_lo, f_hi, m_start)

        m_start += self.n_block_training

        # 2. TOC: encode the training information, modulate it, then insert
        empty_toc = []
        for n in range(5):
            try:
                n_syms_for[n]  # just check if this dest exists
            except KeyError:
                empty_toc.extend((n, 255, 0))

        td = tuple(data_for_toc + empty_toc)
        bytes_toc = TOC.encode(
            modcode_bc, n_bytes_bc,                     # broadcast fields
            *td)                                        # remaining all 0

        # add padding bytes to fill the frame (PAPR control)
        baud_toc = np.empty(n_syms_toc + n_pad_syms_toc, dtype=np.uint8)
        unpack_bytes(bytes_toc + TRAINING_MSG[:n_pad_bytes_toc],
                     ModTable[mod_toc]['bits_per_sym'],
                     baud_toc)
        syms_toc = ModTable[mod_toc]['modulator_obj'].modulate(baud_toc)

        # if __debug__:
        #     print(">build_ofdm_frame: insert ToC - ",
        #           len(syms_toc),
        #           f_lo, f_hi, m_start)

        insert_OFDM(ofdm_frame,
                    syms_toc,
                    f_lo, f_hi, m_start)

        m_start += n_block_toc

        # 3. BC
        if n_bytes_bc > 0:
            baud_bc = np.empty(n_syms_bc + n_pad_syms_bc, dtype=np.uint8)
            unpack_bytes(bc_data['bc_payload'] + TRAINING_MSG[:n_pad_bytes_bc],
                         ModTable[mod_bc]['bits_per_sym'],
                         baud_bc)
            syms_bc = ModTable[mod_bc]['modulator_obj'].modulate(baud_bc)

            # if __debug__:
            #     print(">build_ofdm_frame: insert BC - ",
            #           len(syms_bc),
            #           f_lo, f_hi, m_start)

            insert_OFDM(ofdm_frame,
                        syms_bc,
                        f_lo, f_hi, m_start)

            m_start += n_block_bc

        # 4. data streams
        f_left = f_lo

        if n_block_data == 0:
            return ofdm_frame

        for dest in sorted(data_for.keys()):  # ensure deterministic order
            try:
                f_right = f_left + n_subs_for[dest]
            except Exception as e:
                # print(">>>>>>>>>>caught exception: ", e)
                # print(dest, instr)
                pass

            baud = np.empty(n_syms_for[dest], dtype=np.uint8)
            mod = mod_for[dest]

            unpack_bytes(data_for[dest],
                         ModTable[mod]['bits_per_sym'],
                         baud)
            syms = ModTable[mod]['modulator_obj'].modulate(baud)

            # if __debug__:
            #     print(">build_ofdm_frame: insert data - ",
            #           len(syms),
            #           f_lo, f_hi, m_start)

            insert_OFDM(ofdm_frame,
                        syms,
                        f_left, f_right, m_start)
            f_left = f_right

        return ofdm_frame


if __name__ == '__main__':
    # test data
    instr = {
        'tx_type': 'data',
        'tx_time_s': 1510428347,
        'tx_time_f': 0.09999990463256836,
        'toc_mod': 1,
        'n_fft': 256,
        'f_low': 39,
        'f_high': 189,
        'f_center': -500000.0,
        'BW': 2000000.0,
        'fs': 10000000.0,
        'bc_header': {'bc_payload': bytearray(b'abcdef1234'),
                      'modulation': 1, 'code': 0},
        'payload': [
            {'next_hop_id': 2, 'modulation': 2, 'code': 0,
             'packets': [{'pkt_key': 98, 'n_bytes': 124, 'offset': 2152},
                         {'pkt_key': 99, 'n_bytes': 124, 'offset': 2276}]},
            {'next_hop_id': 3, 'modulation': 2, 'code': 0,
             'packets': [{'pkt_key': 96, 'n_bytes': 124, 'offset': 1904},
                         {'pkt_key': 97, 'n_bytes': 124, 'offset': 2028}]},
            {'next_hop_id': 3, 'modulation': 2, 'code': 0,
             'packets': [{'pkt_key': 90, 'n_bytes': 1240, 'offset': 1000},
                         {'pkt_key': 91, 'n_bytes': 1240, 'offset': 1124}]}
        ]
    }

    # the offset in the payload refers to the starting location of a packet
    # in the packet buffer let's make a large enough buffer with random data
    # for testing
    pkt_buffer = np.random.randint(65, high=122, size=2500, dtype=np.uint8)

    try:
        data_for, mod_for, code_for = make_streams(instr['payload'],
                                                   pkt_buffer)
        print("make_streams ... [ok]")
    except Exception as e:
        print("make_streams ... [not ok] ", e)

    try:
        ns, nb = divide_subcarriers(100, {2: 220, 3: 560})
        print("divide_subcarriers ... [ok]")
    except Exception as e:
        print("divide_subcarriers ... [not ok] ", e)

    try:
        tx = Transmitter(256)
        print("Transmitter() ... [ok]")
    except Exception as e:
        print("Transmitter() ... [not ok] ", e)

    try:
        ofdm_sig = tx.work(instr, pkt_buffer)
        print("Transmitter.work ... [ok]")
    except Exception as e:
        print("Transmitter.work ... [not ok] ", e)

    instr1 = {'fs': 5000000.0,
              'tx_time_s': 1511367080,
              'toc_mod': 1,
              'tx_time_f': 0.2,
              'bc_header': {'code': 0, 'modulation': 1, 'bc_payload': bytearray(b'abcdef1234')},
              'n_fft': 256,
              'payload': [{'code': 0,
                           'modulation': 2,
                           'packets': [{'offset': 2152, 'pkt_key': 98, 'n_bytes': 124}],
                           'next_hop_id': 2}],
              'f_center': 500000.0,
              'f_low': 39,
              'f_high': 89}
    for k in range(10):
        tx.work(instr1, pkt_buffer)

    try:
        from scipy import signal
        import matplotlib.pyplot as plt
        
        f, Pxx_den = signal.welch(ofdm_sig, instr['fs'], nperseg=1024,
                                  return_onesided=False, noverlap=374)

        plt.plot(f/1e6, 10*np.log10(Pxx_den))
        # plt.ylim([0.5e-3, 1])
        plt.xlabel('Frequency [MHz]')
        plt.ylabel('PSD [dB V**2/Hz]')
        plt.grid()
        plt.show()
    except Exception as e:
        pass

		
		
		
		
		
		
		




instr = {
 'tx_type': 'data', 
 'tx_time_s': 1510428347, 
 'tx_time_f': 0.09999990463256836, 
 'toc_mod': 1,
 'n_fft': 256, 
 'f_low': 39, 
 'f_high': 189, 
 'f_center': -500000.0, 
 'BW': 2000000.0, 
 'fs': 10000000.0, 
 'bc_header': {'bc_payload': bytearray(b'abcdef1234'), 'modulation': 1, 'code': 0}, 
 'payload': [{'next_hop_id': 2, 'modulation': 2, 'code': 0, 
              'packets': [{'pkt_key': 98, 'n_bytes': 124, 'offset': 2152},
                          {'pkt_key': 99, 'n_bytes': 124, 'offset': 2276}]},
             {'next_hop_id': 3, 'modulation': 2, 'code': 0, 
              'packets': [{'pkt_key': 96, 'n_bytes': 124, 'offset': 1904},
                          {'pkt_key': 97, 'n_bytes': 124, 'offset': 2028}]},
             {'next_hop_id': 3, 'modulation': 2, 'code': 0, 
              'packets': [{'pkt_key': 90, 'n_bytes': 1240, 'offset': 1000},
                          {'pkt_key': 91, 'n_bytes': 1240, 'offset': 1124}]}]}

pkt_buffer = np.random.randint(65, high=122, size=2500, dtype=np.uint8)		