corSeq = [1 1 1 1 0 0 0 0 1 0 1 0 0 1 0 1];

noiseFactor = 1/30; %controls noise input onto the input data 
padLen = 64; %lenght of data appended to the preamble, doesnt really matter just makesa better test
inputCor = zeros(1, 256);
index = 0;
for i = 1 : length(corSeq) % This just creates the input data that contains the preamble
    for j = 1:16
        index = index + 1;
        inputCor(index) = (rand(1)*noiseFactor) + corSeq(i);
    end
end


inputCor = [rand(1,16 *padLen)*noiseFactor, inputCor];%, ones(1,64 *padLen)*noiseFactor]; %padded with extra bits at the end
inputCor = [inputCor, rand(1,64 *padLen)*noiseFactor];
n = length(inputCor);
m = length(corSeq);

phase0Reg = zeros(1,16);
phase1Reg = zeros(1,16);
phase2Reg = zeros(1,16);
phase3Reg = zeros(1,16);
phase4Reg = zeros(1,16);
phase5Reg = zeros(1,16);
phase6Reg = zeros(1,16);
phase7Reg = zeros(1,16);
phase8Reg = zeros(1,16);
phase9Reg = zeros(1,16);
phase10Reg = zeros(1,16);
phase11Reg = zeros(1,16);
phase12Reg = zeros(1,16);
phase13Reg = zeros(1,16);
phase14Reg = zeros(1,16);
phase15Reg = zeros(1,16);

% 
% for i = 1:16 %first time fill, to fill the buffers with shit for the first time.  Shift will take place later
%     phase0Reg(i) = inputCor((i-1)*16 + 1);
%     phase1Reg(i) = inputCor((i-1)*16 + 2);
%     phase2Reg(i) = inputCor((i-1)*16 + 3);
%     phase3Reg(i) = inputCor((i-1)*16 + 4);
%     phase4Reg(i) = inputCor((i-1)*16 + 5);
%     phase5Reg(i) = inputCor((i-1)*16 + 6);
%     phase6Reg(i) = inputCor((i-1)*16 + 7);
%     phase7Reg(i) = inputCor((i-1)*16 + 8);
%     phase8Reg(i) = inputCor((i-1)*16 + 9);
%     phase9Reg(i) = inputCor((i-1)*16 + 10);
%     phase10Reg(i) = inputCor((i-1)*16 + 11);
%     phase11Reg(i) = inputCor((i-1)*16 + 12);
%     phase12Reg(i) = inputCor((i-1)*16 + 13);
%     phase13Reg(i) = inputCor((i-1)*16 + 14);
%     phase14Reg(i) = inputCor((i-1)*16 + 15);
%     phase15Reg(i) = inputCor((i-1)*16 + 16);
%       
% end

corOut0 = zeros(1,1);
corOut1 = zeros(1,1);
corOut2 = zeros(1,1);
corOut3 = zeros(1,1);
corOut4 = zeros(1,1);
corOut5 = zeros(1,1);
corOut6 = zeros(1,1);
corOut7 = zeros(1,1);
corOut8 = zeros(1,1);
corOut9 = zeros(1,1);
corOut10 = zeros(1,1);
corOut11 = zeros(1,1);
corOut12 = zeros(1,1);
corOut13 = zeros(1,1);
corOut14 = zeros(1,1);
corOut15 = zeros(1,1);


for i = 1:length(inputCor)/16
    phase0Reg = [inputCor((i-1)*16 + 1) , phase0Reg(1:15)]
    phase1Reg = [inputCor((i-1)*16 + 2) , phase1Reg(1:15)]
    phase2Reg = [inputCor((i-1)*16 + 3) , phase2Reg(1:15)]
    phase3Reg = [inputCor((i-1)*16 + 4) , phase3Reg(1:15)]
    phase4Reg = [inputCor((i-1)*16 + 5) , phase4Reg(1:15)]
    phase5Reg = [inputCor((i-1)*16 + 6) , phase5Reg(1:15)]
    phase6Reg = [inputCor((i-1)*16 + 7) , phase6Reg(1:15)]
    phase7Reg = [inputCor((i-1)*16 + 8) , phase7Reg(1:15)]
    phase8Reg = [inputCor((i-1)*16 + 9) , phase8Reg(1:15)]
    phase9Reg = [inputCor((i-1)*16 + 10) , phase9Reg(1:15)]
    phase10Reg = [inputCor((i-1)*16 + 11) , phase10Reg(1:15)]
    phase11Reg = [inputCor((i-1)*16 + 12) , phase11Reg(1:15)]
    phase12Reg = [inputCor((i-1)*16 + 13) , phase12Reg(1:15)]
    phase13Reg = [inputCor((i-1)*16 + 14) , phase13Reg(1:15)]
    phase14Reg = [inputCor((i-1)*16 + 15) , phase14Reg(1:15)]
    phase015Reg = [inputCor((i-1)*16 + 16) , phase15Reg(1:15)]



    corOut0 = [corOut0,sum(phase0Reg .* corSeq)];
    corOut1 = [corOut1,sum(phase1Reg .* corSeq)];
    corOut2 = [corOut2,sum(phase2Reg .* corSeq)];
    corOut3 = [corOut3,sum(phase3Reg .* corSeq)];
    corOut4 = [corOut4,sum(phase4Reg .* corSeq)];
    corOut5 = [corOut5,sum(phase5Reg .* corSeq)];
    corOut6 = [corOut6,sum(phase6Reg .* corSeq)];
    corOut7 = [corOut7,sum(phase7Reg .* corSeq)];
    corOut8 = [corOut8,sum(phase8Reg .* corSeq)];
    corOut9 = [corOut9,sum(phase9Reg .* corSeq)];
    corOut10 = [corOut10,sum(phase10Reg .* corSeq)];
    corOut11 = [corOut11,sum(phase11Reg .* corSeq)];
    corOut12 = [corOut12,sum(phase12Reg .* corSeq)];
    corOut13 = [corOut13,sum(phase13Reg .* corSeq)];
    corOut14 = [corOut14,sum(phase14Reg .* corSeq)];
    corOut15 = [corOut15,sum(phase15Reg .* corSeq)];
    
end
    
plot(corOut1)
figure(2)
plot(corOut7)