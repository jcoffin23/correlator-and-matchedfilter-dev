load('liveDat.mat')
 x=downsample(ImgP,16);



corSeq =[-1. -1. -1. -1.  1.  1.  1.  1. -1.  1. -1.  1.  1. -1.  1. -1.];
syncSeq = [-1.  1.  1. -1. -1. -1.  1.  1.  1.  1. -1. -1.  1. -1. -1. -1.];

x = upsample(corSeq,16);
y = upsample(syncSeq,16);
beSe = conv2(filterSeq',x);
beSy = conv2(filterSeq',y);
figure(1)
subplot(2,1,1)
plot(beSe)
title("filtered CorSeq")
subplot(2,1,2)
plot(beSy)
title("Real data pulse")

out = [beSe,beSy]

% pre = [1 1 1 1 0 0 0 0 1 0 1 0 0 1 0 1]
% sy = [1 0 0 1 1 1 0 0 0 0 1 1 0 1 1 1]% 
% % PREAMBLE_BYTES = b'\xF0\xA5'
% % SYNC_BYTES = b'\x9C\x37'



