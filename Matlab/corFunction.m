function [cor0,cor8] = corFunction(inputCor,corSeq)


n= length(inputCor);
m = length(corSeq);

phase0Reg = zeros(1,16);
phase1Reg = zeros(1,16);
phase2Reg = zeros(1,16);
phase3Reg = zeros(1,16);
phase4Reg = zeros(1,16);
phase5Reg = zeros(1,16);
phase6Reg = zeros(1,16);
phase7Reg = zeros(1,16);
phase8Reg = zeros(1,16);
phase9Reg = zeros(1,16);
phase10Reg = zeros(1,16);
phase11Reg = zeros(1,16);
phase12Reg = zeros(1,16);
phase13Reg = zeros(1,16);
phase14Reg = zeros(1,16);
phase15Reg = zeros(1,16);

corOut0 = zeros(1,1);
corOut1 = zeros(1,1);
corOut2 = zeros(1,1);
corOut3 = zeros(1,1);
corOut4 = zeros(1,1);
corOut5 = zeros(1,1);
corOut6 = zeros(1,1);
corOut7 = zeros(1,1);
corOut8 = zeros(1,1);
corOut9 = zeros(1,1);
corOut10 = zeros(1,1);
corOut11 = zeros(1,1);
corOut12 = zeros(1,1);
corOut13 = zeros(1,1);
corOut14 = zeros(1,1);
corOut15 = zeros(1,1);


for i = 1:length(inputCor)/16

    phase0Reg = [inputCor((i-1)*16 + 1) , phase0Reg(1:15)];
    phase1Reg = [inputCor((i-1)*16 + 2) , phase1Reg(1:15)];
    phase2Reg = [inputCor((i-1)*16 + 3) , phase2Reg(1:15)];
    phase3Reg = [inputCor((i-1)*16 + 4) , phase3Reg(1:15)];
    phase4Reg = [inputCor((i-1)*16 + 5) , phase4Reg(1:15)];
    phase5Reg = [inputCor((i-1)*16 + 6) , phase5Reg(1:15)];
    phase6Reg = [inputCor((i-1)*16 + 7) , phase6Reg(1:15)];
    phase7Reg = [inputCor((i-1)*16 + 8) , phase7Reg(1:15)];
    phase8Reg = [inputCor((i-1)*16 + 9) , phase8Reg(1:15)];
    phase9Reg = [inputCor((i-1)*16 + 10) , phase9Reg(1:15)];
    phase10Reg = [inputCor((i-1)*16 + 11) , phase10Reg(1:15)];
    phase11Reg = [inputCor((i-1)*16 + 12) , phase11Reg(1:15)];
    phase12Reg = [inputCor((i-1)*16 + 13) , phase12Reg(1:15)];
    phase13Reg = [inputCor((i-1)*16 + 14) , phase13Reg(1:15)];
    phase14Reg = [inputCor((i-1)*16 + 15) , phase14Reg(1:15)];
    phase15Reg = [inputCor((i-1)*16 + 16) , phase15Reg(1:15)];

%     figure(1)
%     hold off
%     plot(phase0Reg, 'LineWidth',2)
%     hold on
%     stem(corSeq,'LineWidth',2)
%     pause(.45)

    

    corOut0 = [corOut0,sum(phase0Reg .* corSeq)];
    corOut1 = [corOut1,sum(phase1Reg .* corSeq)];
    corOut2 = [corOut2,sum(phase2Reg .* corSeq)];
    corOut3 = [corOut3,sum(phase3Reg .* corSeq)];
    corOut4 = [corOut4,sum(phase4Reg .* corSeq)];
    corOut5 = [corOut5,sum(phase5Reg .* corSeq)];
    corOut6 = [corOut6,sum(phase6Reg .* corSeq)];
    corOut7 = [corOut7,sum(phase7Reg .* corSeq)];
    corOut8 = [corOut8,sum(phase8Reg .* corSeq)];
    corOut9 = [corOut9,sum(phase9Reg .* corSeq)];
    corOut10 = [corOut10,sum(phase10Reg .* corSeq)];
    corOut11 = [corOut11,sum(phase11Reg .* corSeq)];
    corOut12 = [corOut12,sum(phase12Reg .* corSeq)];
    corOut13 = [corOut13,sum(phase13Reg .* corSeq)];
    corOut14 = [corOut14,sum(phase14Reg .* corSeq)];
    corOut15 = [corOut15,sum(phase15Reg .* corSeq)];
    


%     figure(2)
%     plot(abs(corOut7))
%     x=input('');
end

    cor0 = corOut0;
    cor8 = corOut8;
end

