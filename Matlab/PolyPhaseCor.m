cor1 = load('inputCorrr.dat');
load('liveDat.mat')
MF
cor1 = transpose(cor1);
% corSeq = [1 1 1 1 0 0 0 0 1 0 1 0 0 1 0 1];
corSeq =[-1. -1. -1. -1.  1.  1.  1.  1. -1.  1. -1.  1.  1. -1.  1. -1.];
syncSeq = [-1.  1.  1. -1. -1. -1.  1.  1.  1.  1. -1. -1.  1. -1. -1. -1.];

%  x = upsample(corSeq,16);
corSeq = flip(corSeq);
sySeq = flip(syncSeq);
% inputCor = [beSe,.1*rand(1,4000)];
inputCor = mfout;
% corSeq = flip(corSeq)


[c0,c8] = corFunction(inputCor,corSeq);
[s0,s8] = corFunction(inputCor,sySeq);


result = csvread('result.csv',0,3);
results = csvread('resultSynch.csv',0,3);


figure(3)
subplot(2,1,1)
plot((c0.^2))
hold on
plot(s0.^2)
title('Matlab Poly Phase Correlator')
xlabel('Sample[n]')
ylabel('Correlation Power')

subplot(2,1,2)
plot(result(1:length(c0)))
hold on
plot(results(1:length(c0)))
title('C simulation for Vivado HLS algorithm using Fixed Point')
xlabel('Sample[n]')
ylabel('Correlation Power')
