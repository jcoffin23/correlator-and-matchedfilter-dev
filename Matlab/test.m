f = [0 0.1 .1 1];      % Frequency breakpoints
m = [1 1 0 0];          % Magnitude breakpoints
b = fir2(32,f,m);       % Frequency sampling-based FIR filter design
[h,w] = freqz(b,1,128); % Frequency response of filter
%     plot(f,m,w/pi,abs(h))
%     legend('Ideal','fir2 Designed')
%     title('Comparison of Frequency Response Magnitudes')


fs = 200;

t = 0:1/fs:1;

x = cos(2*pi*10*t);
xx= cos(2*pi*50*t);
xxx = cos(2*pi*25*t);

xsum = x+xx+xxx
plot(t,xsum)
figure(2)

yy = filter(b,1,xsum)
plot(yy)